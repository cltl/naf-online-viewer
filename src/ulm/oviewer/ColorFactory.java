package ulm.oviewer;

import java.util.Map;

import com.google.common.collect.Maps;

public class ColorFactory {

	private static final String[] DARK_COLORS = { "Aqua", "Blue", "BlueViolet",
			"Brown", "CadetBlue", "Chocolate", "CornflowerBlue", "Crimson",
			"DarkBlue", "DarkCyan", "DarkGoldenRod", "DarkGreen",
			"DarkMagenta", "DarkOliveGreen", "DarkOrchid", "DarkRed",
			"DarkSlateBlue", "DarkSlateGray", "DarkTurquoise", "DarkViolet",
			"DeepSkyBlue", "DodgerBlue", "FireBrick", "Green", "Indigo",
			"Magenta", "Maroon", "MediumBlue", "MediumOrchid", "MidnightBlue",
			"RebeccaPurple", "RoyalBlue", "SteelBlue", };

	private static final String[] LIGHT_COLORS = { "Aqua",
			"Beige", "Chartreuse", "Cyan", "Gainsboro", "Gold", "GreenYellow",
			"Lavender", "LemonChiffon", "LightCyan", "LightGreen", "Orange",
			"red", "PaleGreen", "PaleTurquoise", "OrangeRed", "Bisque",
			"PowderBlue", "SkyBlue", "Tomato", "Violet", "Turquoise", "Orchid",
			"Yellow", "YellowGreen" };
	
	private static final String[] COLORS = LIGHT_COLORS;
	
	private Map<String, String> map = Maps.newHashMap();
	private String[] colorRepo = new String[COLORS.length];
	
	public synchronized String get(String name) {
		if (!map.containsKey(name)) {
			if (isAllNull(colorRepo)) {
				System.arraycopy(COLORS, 0, colorRepo, 0, COLORS.length);
			}
			int index = Math.abs(name.hashCode());
			int offset = 0;
			while (offset < colorRepo.length &&
					colorRepo[(index+offset) % colorRepo.length] == null) {
				offset += 1;
			}
			if (offset >= colorRepo.length) {
				throw new IllegalStateException("Not enough colors.");
			}
			map.put(name, colorRepo[(index+offset) % colorRepo.length]);
			colorRepo[(index+offset) % colorRepo.length] = null;
		}
		return map.get(name);
	}
	
	private boolean isAllNull(Object[] arr) {
		for (int i = 0; i < arr.length; i++) {
			if (arr[i] != null) return false;
		}
		return true;
	}
	
}
