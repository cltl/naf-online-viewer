package ulm.oviewer;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jdom2.JDOMException;
import org.junit.Test;

import com.google.common.base.Charsets;
import com.google.common.io.Resources;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class NAF2HTMLTest {

    @Test
    public void empty() throws IOException, JDOMException {
        String nafStr = Resources.toString(
                Resources.getResource(NAF2HTMLTest.class, "empty.naf"),
                Charsets.UTF_8);
        String html = new NAF2HTML().viewLayerAsDiv(nafStr, "entities");
        assertTrue(Pattern.matches("<div></div>|<div\\s*/>", html));
    }

    @Test
    public void oneEntity() throws IOException, JDOMException {
        String nafStr = Resources.toString(
                Resources.getResource(NAF2HTMLTest.class, "one-entity.naf"),
                Charsets.UTF_8);
        String html = new NAF2HTML().viewLayerAsDiv(nafStr, "entities");
        String regex = "<span .*?class=\"annotation\".*?>(.+?)</span>";
        Matcher matcher = Pattern.compile(regex).matcher(html);
        assertTrue(matcher.find());
        assertEquals("American", matcher.group(1));
    }
    
    @Test
    public void realData() throws IOException, JDOMException {
        String fileName = "47K9-W260-006F-01XY.xml_dc767673993538525fb781e3dff2b0aa.naf.coref";
        String nafStr = Resources.toString(
                Resources.getResource(NAF2HTMLTest.class, fileName),
                Charsets.UTF_8);
        NAF2HTML naf2html = new NAF2HTML();
        assertTrue(naf2html.viewLayerAsDiv(nafStr, "text").length() > 1000);
        assertTrue(naf2html.viewLayerAsDiv(nafStr, "terms").length() > 1000);
        assertTrue(naf2html.viewLayerAsDiv(nafStr, "entities").length() > 1000);
    }

}
