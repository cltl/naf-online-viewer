## Installation

TODO

## Development

The tool was developed on Eclipse 4.4.0 (Java EE) using Tomcat 7.0.55.

## Usage

Open index.jsp, input NAF string or **absolute** URL to a NAF file and select a
layer to display.

After submitting, you will see a page displaying the document with annotations 
marked by background color. Hover the mouse on an annotation to see more 
details. Some layers differentiate more than one types of annotations, they are
highlighted by different colors. For example, persons and organizations appear
differently.

You can change the displayed layer at any time.

## Known issues or future development

* Overlapping annotation may be displayed incorrectly.
* Only one layer at a time.