<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.io.File"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="naf" uri="WEB-INF/naf.tld"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="main.css">
<%
String path = request.getParameter("local_path");
if (path == null || path.isEmpty()) {
%>
    <title>NAF Online Viewer</title>
<%
} else {
	String name = new File(path).getName();
%>
    <title><%= name %> - NAF Online Viewer</title>
<%
}
%>
<script type="text/javascript">
function registerDynamicAnnotation(headClass, dependentId, bgcolor, fgcolor) {
	heads = document.getElementsByClassName(headClass);
	dependent = document.getElementById(dependentId);
	for (i = 0; i < heads.length; i++) {
		head = heads[i];
	    head.addEventListener("mouseover", (function() {
	    	var privateDependent = dependent;
	    	return function() {
		        privateDependent.style.backgroundColor = bgcolor;
		        privateDependent.style.color = fgcolor;
		    };
	    })());
	    head.addEventListener("mouseout", (function() {
            var privateDependent = dependent;
            var oldBgColor = dependent.style.backgroundColor;
            var oldFgColor = dependent.style.color
            return function() {
		        privateDependent.style.backgroundColor = oldBgColor;
		        privateDependent.style.color = oldFgColor;
            };
        })());
	} 
}
</script>
</head>
<body>
<div style="float: right">
<a href="index.jsp">Home</a> -
<a href="help.html" target="_blank">Help</a>
</div>
<form action="view.jsp" method="post">
<label>Choose a layer: <jsp:include page="modes.jsp"/></label>
<input type="submit" value="View"/>
<input type="hidden" name="url" value="${param.url}"/>
<input type="hidden" name="local_path" value="${param.local_path}"/>
<input type="hidden" name="string" value="${fn:escapeXml(param.string)}"/>
</form>
<hr/>
<%
if (!(path == null || path.isEmpty())) {
	 %>
    <p><strong><%= path %></strong></p>
    <%
}
%>
<naf:layer name="${param.mode}" 
        localPath="${param.local_path}" url="${param.url}" 
        string="${param.string}"/>
</body>
</html>