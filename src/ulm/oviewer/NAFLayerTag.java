package ulm.oviewer;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.net.URL;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import org.jdom2.JDOMException;

import com.google.common.base.Charsets;
import com.google.common.base.Strings;
import com.google.common.io.Files;
import com.google.common.io.Resources;

public class NAFLayerTag extends SimpleTagSupport {

    private String name;
    private String string;
    private String url;
    private String localPath;
    private NAF2HTML naf2html = new NAF2HTML();
    
    public void doTag() throws JspException, IOException {
        try {
            String naf;
            if (!Strings.isNullOrEmpty(string)) {
                naf = string;
            } else if (!Strings.isNullOrEmpty(url)) {
                naf = Resources.toString(new URL(url), Charsets.UTF_8);
            } else if (!Strings.isNullOrEmpty(localPath)) {
                naf = Files.toString(new File(localPath), Charsets.UTF_8);
            } else {
                StringWriter sw = new StringWriter();
                getJspBody().invoke(sw);
                naf = sw.toString();
            }
            String div = naf2html.viewLayerAsDiv(naf, getName());
            getJspContext().getOut().println(div);
        } catch (JDOMException e) {
            throw new JspException(e);
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getLocalPath() {
		return localPath;
	}

	public void setLocalPath(String localPath) {
		this.localPath = localPath;
	}

	public String getString() {
        return string;
    }

    public void setString(String string) {
        this.string = string;
    }

}