package ulm.oviewer;

import java.util.HashMap;
import java.util.Map;

import com.google.common.collect.Maps;

public class AbbreviationMap extends HashMap<String, String> {

	private static final long serialVersionUID = 1L;
	
	private Map<String, String> rmap = Maps.newHashMap();
	
	@Override
	public String get(Object key) {
		String ret = super.get(key);
		if (ret == null && key instanceof String) return (String) key;
		return ret;
	}
	
	@Override
	public String put(String key, String value) {
		String ret = super.put(key, value); 
		resolveConflicts(key, value);
		return ret;
	}
	
    /**
     * Ensure that all values are unique. Rename values with name X in to
     * X1, X2,...
     * @param map
     */
    private void resolveConflicts(String key, String value) {
		if (rmap.containsKey(value) && !key.equals(rmap.get(value))) { // clash
//			if (rmap.get(value) != null) { // unresolved
//				// resolve it: change value and mark as resolved
//				String clashedKey = rmap.get(value);
//				super.put(clashedKey, value + 1);
//				rmap.put(value, null);
//			}
			// find an available slot
			int i = 2; 
			while (rmap.containsKey(value+i)) i++;
			value = value + i;
		}
		super.put(key, value);
		rmap.put(value, key);
    }

}
