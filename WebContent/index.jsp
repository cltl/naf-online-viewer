<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="naf" uri="WEB-INF/naf.tld"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="main.css">
<title>NAF Online Viewer</title>
</head>
<body>
<form action="view.jsp" method="post">
<p><label>Choose a mode: <jsp:include page="modes.jsp"/></label></p>
<p><label>Enter URL to NAF file: <input name="url"/></label></p>
<p><label>Or choose: 
<select name="local_path">
    <naf:file-options/>
</select></label></p>
<p><label>Or copy-paste a NAF string: <textarea name="string" cols="25" rows="20"></textarea></label></p>
<p><input type="submit" value="Submit"/></p>
</form>
</body>
</html>