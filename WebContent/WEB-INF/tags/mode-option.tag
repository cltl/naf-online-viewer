<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ attribute name="value" required="true" %>

<option value="${value}" ${param.mode == value ? 'selected="selected"' : ''}>
<jsp:doBody></jsp:doBody>
</option>
