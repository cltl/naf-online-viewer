package ulm.oviewer;

import java.util.Comparator;

public class Annotation {
    int start;
    int end;
    String content;
    String type;
    boolean superscript;
    
    public Annotation(int start, int end, String content) {
        this(start, end, content, null);
    }
    
    public Annotation(int start, int end, String content, String type) {
    	this(start, end, content, type, false);
    }
        
    public Annotation(int start, int end, String content, String type, boolean superscript) {
        super();
        this.start = start;
        this.end = end;
        this.content = content;
        this.type = type;
		this.superscript = superscript;
    }
    
    @Override
    public String toString() {
    	return String.format("[%d-%d] %s", start, end, type);
    }
    
    public static Comparator<Annotation> OFFSET_COMPARATOR = new AnnotationOffsetComparator();
    
    private static class AnnotationOffsetComparator implements Comparator<Annotation> {
        @Override
        public int compare(Annotation o1, Annotation o2) {
        	int c = Integer.compare(o1.start, o2.start);
        	if (c == 0) {
        		c = Integer.compare(o1.end, o2.end);
        	}
            return c;
        }
    }
}