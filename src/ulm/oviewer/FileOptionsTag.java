package ulm.oviewer;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.regex.Pattern;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import com.google.common.io.PatternFilenameFilter;

public class FileOptionsTag extends SimpleTagSupport {

	public void doTag() throws JspException, IOException {
		String path = "/home/minh/corpus_airbus_NAF";
		JspWriter out = getJspContext().getOut();
		File[] files = new File(path).listFiles(new PatternFilenameFilter(
				Pattern.compile(".+\\.naf", Pattern.CASE_INSENSITIVE)));
		if (files != null) {
			Collections.sort(Arrays.asList(files), new Comparator<File>() {

				@Override
				public int compare(File f1, File f2) {
					return f1.getName().compareTo(f2.getName());
				}
			});
			for (File file : files) {
				out.println(String.format("<option value=%s>%s</option>",
						file.getAbsolutePath(), file.getName()));
			}
		} else {
			out.println("<option disabled>[nothing to choose from]</option>");
		}
	}

}