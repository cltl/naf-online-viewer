<%@ taglib prefix="ct" tagdir="/WEB-INF/tags" %>

<select name="mode">
<ct:mode-option value="text">Text</ct:mode-option>
<ct:mode-option value="terms">Terms</ct:mode-option>
<ct:mode-option value="ner">NER</ct:mode-option>
<!-- 
<ct:mode-option value="semeval15">semeval 2015 (wsd+ned+coref)</ct:mode-option>
-->
<ct:mode-option value="timex3">Timex3</ct:mode-option>
<ct:mode-option value="ned">NED</ct:mode-option>
<ct:mode-option value="babelfy">Babelfy Replication Experiment</ct:mode-option>
<!-- 
<ct:mode-option value="coreferences">coreferences</ct:mode-option>
<ct:mode-option value="deps">deps</ct:mode-option>
<ct:mode-option value="factuality">factuality</ct:mode-option>
<ct:mode-option value="opinions">opinions</ct:mode-option>
-->
<ct:mode-option value="srl">SRL</ct:mode-option>
</select>