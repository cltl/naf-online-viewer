package ulm.oviewer;

import java.io.IOException;
import java.io.StringReader;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.filter.Filters;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.XMLOutputter;
import org.jdom2.xpath.XPathExpression;
import org.jdom2.xpath.XPathFactory;

import com.google.common.base.Joiner;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

public class NAF2HTML {
	
	// to fix faulty offsets in some NAF files
	private ColorFactory colors = new ColorFactory();

    public String viewLayerAsDiv(String nafStr, String mode)
            throws JDOMException, IOException {
        SAXBuilder jdomBuilder = new SAXBuilder();
        Document document = jdomBuilder.build(new StringReader(nafStr));
        fixOffsets(document);

        // for easier and faster retrieval of elements
        Map<String, Element> id2Elem = Maps.newHashMap();
        addChildrenToMap(document.getRootElement().getChild("text"), id2Elem);
        addChildrenToMap(document.getRootElement().getChild("terms"), id2Elem);

        String html = "";
        html += renderPropertiesView(document, mode, id2Elem);
        html += renderAnnotationView(document, mode, id2Elem);
        return html;
    }
    
    private void fixOffsets(Document document) {
        String raw = getRawText(document);
        List<Element> wfs = document.getRootElement().getChild("text").getChildren("wf");
        int[] d = {0, 1, -1, 2, -2, 3, -3, 4, -4};
        int drift = 0;
    	for (Element wf : wfs) {
			int s = Integer.parseInt(wf.getAttributeValue("offset")); // start
			int e = s+Integer.parseInt(wf.getAttributeValue("length")); // end
    		for (int i = 0; i < d.length; i++) {
    			int new_drift = drift + d[i];
    			try {
	    			if (wf.getText().equals(raw.substring(s+new_drift, e+new_drift))) {
	    				drift = new_drift;
	    				break;
	    			}
    			} catch (StringIndexOutOfBoundsException _) {}
    		}
    		wf.setAttribute("offset", String.valueOf(s+drift));
    	}
	}

	private String renderAnnotationView(Document document, String mode,
    		Map<String, Element> id2Elem) {
        String raw = getRawText(document);
        List<Annotation> annotations = Lists.newArrayList();
        switch (mode) {
        case "text":
            words2Annotations(document, annotations);
            break;
        case "terms":
            terms2Annotations(document, annotations, id2Elem);
            break;
        case "ner":
            entities2Annotations(document, annotations, id2Elem);
            break;
        case "semeval15":
            wordSenses2Annotations(document, annotations, id2Elem);
            entityLinks2Annotation(document, annotations, id2Elem, "reranker");
            break;
        case "ned":
            entityLinks2Annotation(document, annotations, id2Elem, null);
            break;
        case "babelfy":
            entityLinks2Annotation(document, annotations, id2Elem, "babelfy");
            break;
        case "srl":
            semanticRoles2Annotation(document, annotations, id2Elem, null);
            break;
        case "timex3":
        	timeExpr2Annotations(document, annotations, id2Elem);
        	break;
        }
        Collections.sort(annotations, Annotation.OFFSET_COMPARATOR);
        mergeAnnotations(annotations);
        switch (mode) {
        case "srl":
        	return renderDynamicAnnotations(raw, annotations);
    	default:
    		return renderStaticAnnotations(raw, annotations);
        }
    }
    
    private String renderPropertiesView(Document document, String mode,
    		Map<String, Element> id2Elem) {
    	String content = "";
        switch (mode) {
        case "text":
            break;
        case "terms":
            break;
        case "ner":
            break;
        case "semeval15":
            break;
        case "ned":
            break;
        case "srl":
            break;
        case "timex3":
        	content = getTimexProperties(document, id2Elem);
        }
        if (content.isEmpty()) {
        	return "";
        }
        return "<div style=\"float: right; width=200px\">" + content + "</div>";
    }
    
	private String getTimexProperties(Document document,
			Map<String, Element> id2Elem) {
		//TODO
		return "";
	}

	private void mergeAnnotations(List<Annotation> annotations) {
		for (int i = 0; i < annotations.size()-1; i++) {
			Annotation a = annotations.get(i);
			Annotation b = annotations.get(i+1);
			while (a.start == b.start && a.end == b.end && a.start != a.end) {
				String content = joinNullSafe("\n", a.content, b.content);
				String type = a.type + " " + b.type;
				if (a.superscript != b.superscript) {
					throw new IllegalArgumentException();
				}
				a = new Annotation(a.start, a.end, content, type, a.superscript);
				annotations.remove(i+1);
				annotations.set(i, a);
				if (annotations.size() > i+1) {
					b = annotations.get(i+1);
				} else {
					break;
				}
			}
		}
	}

	private String joinNullSafe(String joiner, String... parts) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < parts.length; i++) {
			if (parts[i] != null) {
				if (sb.length() > 0) {
					sb.append(joiner);
				}
				sb.append(parts[i]);
			}
		}
		if (sb.length() <= 0) { // all parts are null
			return null;
		}
		return sb.toString();
	}

	private String renderStaticAnnotations(String raw, List<Annotation> annotations) {
		// use JDOM instead of StringBuilder to escape XML properly
        Element div = new Element("div");
        try {
	        int lastOffset = 0;
	        for (Annotation ann : annotations) {
	            if (lastOffset < ann.start) {
	                div.addContent(raw.substring(lastOffset, ann.start));
	            }
	            if (!ann.superscript) {
		            Element span = new Element("span");
		            span.setAttribute("class", "annotation");
		            if (!Strings.isNullOrEmpty(ann.type)) {
		            	String firstType = ann.type.split(" ")[0];
		            	span.setAttribute("style", "background-color:" + colors.get(firstType));
		            }
		            if (!Strings.isNullOrEmpty(ann.content)) {
		            	span.setAttribute("title", ann.content);
		            }
		            span.setText(raw.substring(ann.start, ann.end));
		            div.addContent(span);
	            } else {
	                Element sup = new Element("sup");
	                sup.setText(ann.content);
	                div.addContent(sup);
	            }
	            lastOffset = ann.end;
	        }
	        if (lastOffset < raw.length()) {
	            div.addContent(raw.substring(lastOffset));
	        }
        } catch (StringIndexOutOfBoundsException e) {
        	// because NAF files are messy
        	div.addContent(String.format(" [%s]", e.getMessage()));
        }
        
        // post-process
        String html = new XMLOutputter().outputString(div);
        html = html.replaceAll("\r?\n", "<br/>");
        return html;
	}

	private String renderDynamicAnnotations(String raw, List<Annotation> annotations) {
		// use JDOM instead of StringBuilder to escape XML properly
        Element div = new Element("div");
        int lastOffset = 0;
        String script = "";
        int id = 0;
        try {
	        for (Annotation ann : annotations) {
	        	String annId = "ann" + id;
	            if (lastOffset < ann.start) {
	                div.addContent(raw.substring(lastOffset, ann.start));
	            }
	            if (!ann.superscript) {
		            Element span = new Element("span");
		            span.setAttribute("id", annId);
		            String classStr = "annotation-dynamic";
		            if (!Strings.isNullOrEmpty(ann.type)) {
		            	for (String type : ann.type.split(" ")) {
		            		if (type.startsWith("!")) {
		            			classStr += " " + type.substring(1);
		            		} else if (type.contains("@")) {
		            			String[] chunks = type.split("@");
		            			String bgcolor = colors.get(chunks[0]);
		            			script += String.format("registerDynamicAnnotation"
		            					+ "('%s', '%s', '%s', '');\n", 
		            					chunks[1], annId, bgcolor);
		            		} else {
		            			span.setAttribute("style", "background-color:" + 
		            					colors.get(type));
		            		}
		            	}
		            }
		            if (!Strings.isNullOrEmpty(ann.content)) {
		            	span.setAttribute("title", ann.content);
		            }
		            span.setAttribute("class", classStr);
		            if (!Strings.isNullOrEmpty(ann.content)) {
		            	span.setAttribute("title", ann.content);
		            }
		            span.setText(raw.substring(ann.start, ann.end));
		            div.addContent(span);
	            } else {
	                Element sup = new Element("sup");
	                if (ann.type.startsWith("!")) {
	                	sup.setAttribute("class", ann.type.substring(1));
	                } else {
	                	sup.setAttribute("style", "color:#F0F0F0");
	                }
		            sup.setAttribute("id", annId);
		            if (!Strings.isNullOrEmpty(ann.type)) {
		                if (ann.type.contains("@")) {
		        			String[] chunks = ann.type.split("@");
		        			script += String.format("registerDynamicAnnotation"
		        					+ "('%s', '%s', '', 'red');", 
		        					chunks[1], annId);
		        		}
		            }
	                sup.setText(ann.content);
	                div.addContent(sup);
	            }
	            lastOffset = ann.end;
	            id += 1;
	        }
	        if (lastOffset < raw.length()) {
	            div.addContent(raw.substring(lastOffset));
	        }
        } catch (StringIndexOutOfBoundsException e) {
        	// because NAF files are messy
        	div.addContent(String.format(" [%s]", e.getMessage()));
        }
        
        // post-process
        String html = new XMLOutputter().outputString(div);
        html = html.replaceAll("\r?\n", "<br/>");
        html += "<script>" + script + "</script>";
        return html;
	}

    public String getRawText(Document document) {
        Element rawElem = document.getRootElement().getChild("raw");
        String raw;
        if (rawElem != null) {
            raw = rawElem.getText();
        } else {
        	// some NAF file doesn't have a "raw" layer, we need to reconstruct
        	// the raw text from "text" layer
        	StringBuilder sb = new StringBuilder();
            Element layer = document.getRootElement().getChild("text");
            for (Element wf : layer.getChildren()) {
            	// assume tokens are in correct order
            	int offset = Integer.parseInt(wf.getAttributeValue("offset"));
				while (sb.length() < offset) sb.append(' ');
				sb.append(wf.getText());
            }
            raw = sb.toString();
        }
        return raw;
    }

    private void entities2Annotations(Document document,
            List<Annotation> annotations, Map<String, Element> id2Elem) {
        Element layer = document.getRootElement().getChild("entities");
        for (Element entity : layer.getChildren()) {
            String annContent = "Entity: " + 
	            	"id=" + entity.getAttributeValue("id") + ", " +
	            	"type=" + entity.getAttributeValue("type");

        	int[][] spans = parseTermSpans(entity.getChild("references")
        			.getChild("span"), id2Elem);
        	annotateAllSpans(spans, annContent, entity.getAttributeValue("type"), 
        			annotations);
        }
    }

    private static void terms2Annotations(Document document,
            List<Annotation> annotations, Map<String, Element> id2Elem) {
        Element layer = document.getRootElement().getChild("terms");
        for (Element term : layer.getChildren()) {
        	String id = term.getAttributeValue("id");
        	String pos = term.getAttributeValue("pos");
        	String morphofeat = term.getAttributeValue("morphofeat");
        	String lemma = term.getAttributeValue("lemma");
        	
            String annContent = String.format(
            		"%s: lemma=%s, pos=%s, morphofeat=%s", 
            		id, lemma, pos, morphofeat);
            int[][] spans = parseTermSpans(term.getChild("span"), id2Elem);
            annotateAllSpans(spans, annContent, pos, annotations);
        }
    }

    private static void words2Annotations(Document document,
            List<Annotation> annotations) {
        Element layer = document.getRootElement().getChild("text");
        int count = 0;
        for (Element word : layer.getChildren()) {
            int start = Integer.parseInt(word.getAttributeValue("offset"));
            int end = start + Integer.parseInt(word.getAttributeValue("length"));
            annotations.add(new Annotation(start, end, null, 
            		(count % 2==0 ? "EVEN" : "ODD")));
            count += 1;
        }
    }

	private void entityLinks2Annotation(Document document,
			List<Annotation> annotations, Map<String, Element> id2Elem,
			String resource) {
		String query;
		if (resource == null) {
			query = ".//externalRef";
		} else {
			query = String.format(".//externalRef[@resource='%s']", resource);
		}
		XPathExpression<Element> xpe = XPathFactory.instance().compile(query, Filters.element());
		
        Element layer = document.getRootElement().getChild("entities");
        if (layer == null) return;
        
        // add annotations
        AbbreviationMap url2abbr = new AbbreviationMap();
        for (Element entity : layer.getChildren()) {
        	List<Element> externalRefs = xpe.evaluate(entity);
        	Element extRef = null;
        	if (externalRefs.size() > 1) {
        		float bestConfidence = 0;
	        	for (Element r : externalRefs) {
	        		float confidence = Float.parseFloat(r.getAttributeValue("confidence"));
	        		if (confidence > bestConfidence) {
	        			extRef = r;
	        			bestConfidence = confidence;
	        		}
	        	}
        	} else if (externalRefs.size() == 1) {
        		extRef = externalRefs.get(0);
        	} else {
        		continue;
        	}

        	String url = extRef.getAttributeValue("reference");
        	String annContent = "Entity: " + url;
        	
        	int[][] spans = parseTermSpans(entity.getChild("references")
        			.getChild("span"), id2Elem);
        	annotateAllSpans(spans, annContent, url, annotations);
    		Matcher matcher = Pattern.compile("http://dbpedia.org/resource/([^/]+)$", 
    				Pattern.CASE_INSENSITIVE).matcher(url);
    		if (matcher.find()) {
    			String shortName = matcher.group(1).substring(0, 3);
    			url2abbr.put(url, shortName);
			}
        	int[] lastSpan = spans[spans.length-1];
        	annotations.add(new Annotation(lastSpan[1], lastSpan[1], 
        			url2abbr.get(url), null, true));
        }
	}

	private void semanticRoles2Annotation(Document document,
			List<Annotation> annotations, Map<String, Element> id2Elem,
			String resource) {
        Element layer = document.getRootElement().getChild("srl");
        if (layer == null) return;
        String query = "./externalReferences/externalRef";
        XPathExpression<Element> xpe = XPathFactory.instance().compile(query, Filters.element());
	
        
        // add annotations
        for (Element predicateElem : layer.getChildren()) {
        	// annotate predicate
        	String predicateId = predicateElem.getAttributeValue("id");
        	Map<String, String> predicateRefs = Maps.newTreeMap(); // remember in order
        	for (Element extRefElem : xpe.evaluate(predicateElem)) {
        		predicateRefs.put(extRefElem.getAttributeValue("resource"),
        				extRefElem.getAttributeValue("reference"));
        	}
        	String predicateContent = predicateId + ": ";
        	if (predicateRefs.isEmpty()) {
        		predicateContent += "#NOREF";
        	} else {
        		predicateContent += Joiner.on(" ").join(predicateRefs.values());
        	}
        	int[][] predicateSpans = parseTermSpans(predicateElem.getChild("span"), id2Elem);
        	annotateAllSpans(predicateSpans, predicateContent, "PRED !" + predicateId, annotations);
        	int[] lastPredicateSpan = predicateSpans[predicateSpans.length-1];
			annotations.add(new Annotation(lastPredicateSpan[1], lastPredicateSpan[1], 
					predicateId, "!" + predicateId, true));
			
			// annotate roles
			for (Element roleElem : predicateElem.getChildren("role")) {
				String role = roleElem.getAttributeValue("semRole");
	        	Map<String, String> roleRefs = Maps.newTreeMap(); // remember in order
	        	for (Element extRefElem : xpe.evaluate(roleElem)) {
	        		roleRefs.put(extRefElem.getAttributeValue("resource"),
	        				extRefElem.getAttributeValue("reference"));
	        	}
	        	String roleContent = role + "@" + predicateId + ": ";
	        	if (roleRefs.isEmpty()) {
	        		roleContent += "#NOREF";
	        	} else {
	        		roleContent += Joiner.on(" ").join(roleRefs.values());
	        	}
	        	int[][] roleSpans = parseTermSpans(roleElem.getChild("span"), id2Elem);
	        	annotateAllSpans(roleSpans, roleContent, role + "@" + predicateId, annotations);
	        	int[] lastRoleSpan = roleSpans[roleSpans.length-1];
				annotations.add(new Annotation(lastRoleSpan[1], lastRoleSpan[1], 
						role, "@" + predicateId, true));
			}
        }
	}

	private static int[][] parseTermSpans(Element span, Map<String, Element> id2Elem) {
		if (span == null) {
			return new int[0][];
		}
		List<int[]> spans = Lists.newArrayList();
		List<Element> targets = span.getChildren("target");
    	for (Element target : targets) {
    		Element term = id2Elem.get(target.getAttributeValue("id"));
    		spans.addAll(Arrays.asList(parseWordSpans(term.getChild("span"), id2Elem)));
    	}
    	return spans.toArray(new int[spans.size()][]);
	}
	
	private static int[][] parseWordSpans(Element span, Map<String, Element> id2Elem) {
		if (span == null) {
			return new int[0][];
		}
		List<int[]> spans = Lists.newArrayList();
		List<Element> wordRefs = span.getChildren("target");
		for (Element wordRef : wordRefs) {
			Element word = id2Elem.get(wordRef.getAttributeValue("id"));
			int start = Integer.parseInt(word.getAttributeValue("offset"));
			int end = start + Integer.parseInt(word.getAttributeValue("length"));
			spans.add(new int[] {start, end});
		}
		return spans.toArray(new int[spans.size()][]);
	}

	private static void annotateAllSpans(int[][] spans, String content, String type,
			List<Annotation> annotations) {
		for (int i = 0; i < spans.length; i++) {
			int start = spans[i][0];
			int end = spans[i][1];
			annotations.add(new Annotation(start, end, content, type));
		}
	}

    private void timeExpr2Annotations(Document document,
            List<Annotation> annotations, Map<String, Element> id2Elem) {
        Element layer = document.getRootElement().getChild("timeExpressions");
        for (Element timeElem : layer.getChildren()) {
            String annContent = String.format("%s [%s]: %s", 
            		timeElem.getAttributeValue("id"),
            		timeElem.getAttributeValue("type"),
            		timeElem.getAttributeValue("value"));
        	int[][] spans = parseWordSpans(timeElem.getChild("span"), id2Elem);
        	annotateAllSpans(spans, annContent, timeElem.getAttributeValue("type"), 
        			annotations);
        }
    }

	private void wordSenses2Annotations(Document document,
			List<Annotation> annotations, Map<String, Element> id2Elem) {
		String query = ".//externalRef[contains(@resource,'ItMakesSense')]";
		XPathExpression<Element> xpe = XPathFactory.instance().compile(query, Filters.element());
        Element layer = document.getRootElement().getChild("terms");
        if (layer == null) return;
        for (Element entity : layer.getChildren()) {
//			sense_key = externalRef_elem.attrib['reference']
//        	score = float(externalRef_elem.attrib['confidence'])
        }
	}

	private void addChildrenToMap(Element elem, Map<String, Element> map) {
        if (elem == null) {
            return;
        }
        for (Element child : elem.getChildren()) {
            map.put(child.getAttributeValue("id"), child);
        }
    }

}